# Overview
This HowTo describes how the gnome3 dash (reachable defaultly via top-left corner activities button) can be put statically left to dock (look as unity bar) in archlinux

The end result looks like this:


![Result Dock](images/dock.png  "Result Dock")


## HowTo

1. Install Firefox extension `gnome-shell-integration` from `https://addons.mozilla.org/de/firefox/addon/gnome-shell-integration/`

2. Install `gnome-browser-connector` via pacman and restart Firefox

```sh
sudo pacman -S gnome-browser-connector
```

3. Install `Dash to Dock` from `https://extensions.gnome.org/extension/307/dash-to-dock/` by toggling the lever

![Dash to Dock](images/dash_to_dock.png "Dash to Dock")


4. Then click on the button right to the lever to open the settings

5. Set the position to left and you can change the sizes

![Dock Position and Size](images/d2d_position.png "Dock Position and Size")

6. Set the Click action behavior to "Show window previews"

![Dock Behavior](images/d2d_click_action.png "Dock Behavior")

7. Set the windows counter indicator appearance to "Dots"

![Dock Appearance](images/d2d_windows_counter_indicator.png "Dock Appearance")


